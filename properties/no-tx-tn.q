/* Node i never transmits within Tx */
/* Verified for N = 4 */
E<> exists(i : int[0,N-1])(y >= Tn && Node(i).tx_ever_occurred == false)
