#!/bin/bash

DIR=~/model-checking-802.11-fd/properties
FILE=~/model-checking-802.11-fd/csma-ca_802.11.xml
UPPAAL=~/uppaal64-4.1.19/bin-Linux/verifyta

# Set N to the maximum number of nodes the property has been satisfied with
# sed -i 's/const int N = ./const int N = 2/g' $FILE

# Use nohup to avoid process kill when ssh is disconnected
time $UPPAAL -S2 -s -u $FILE -q $DIR/k-tx-tn.q -f $DIR/k-tx-tn.xtr -t1 > out-k-tx-tn.txt &
