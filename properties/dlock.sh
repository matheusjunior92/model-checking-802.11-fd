#!/bin/bash

DIR=~/model-checking-802.11-fd/properties
FILE=~/model-checking-802.11-fd/csma-ca_802.11.xml
UPPAAL=~/uppaal64-4.1.19/bin-Linux/verifyta

# Set N to the maximum number of nodes the property has been satisfied with
# sed -i 's/const int N = ./const int N = 2/g' $FILE

# Use nohup to avoid process kill when ssh is disconnected
nohup $UPPAAL -A -S2 -s -u $FILE -q $DIR/dlock.q -f $DIR/dlock.xtr -t1 > out-dlock.txt &
