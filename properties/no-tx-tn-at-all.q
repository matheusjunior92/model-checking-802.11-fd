/* Node i never transmits within Tx */
/* Verified for N = 4 */
E<> forall(i : int[0,N-1])(y >= Tn && Node(i).numOfTx == 0)
