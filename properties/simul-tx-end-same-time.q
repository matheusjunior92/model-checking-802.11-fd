/* Simultaneous transmission */
/* Verified for N = 2 */
E<> exists(i:int[0,N-1]) (Node(i).Transmitting_Data_PTX && Node(not i).Transmitting_Data_STX) && (Node(i).x == TPayload && Node(not i).x == TPayload)
