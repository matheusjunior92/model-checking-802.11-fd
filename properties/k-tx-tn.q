/* Is it possible for node i to transmit k times within Tn */
/* Verified for N = 2 */
E<> exists(i : int[0,N-1])(y <= Tn && Node(i).numOfTx >= 2 && Node(i).numOfTx <= N)
